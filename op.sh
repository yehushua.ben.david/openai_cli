#!/bin/bash

show_help() {
	echo "Usage: $0 [OPTIONS] [TEXT]"
	echo
	echo "OPTIONS:"
	echo "  --help                  Show this help message"
	echo
	echo "TEXT:"
	echo "  Text input for the OpenIA chatbot"
	echo "  If no TEXT is provided, the script will prompt for input"
}

if [[ "$1" == "--help" ]]; then
	show_help
	exit 0
fi

if ! [ -f ~/.op/API_KEY ]; then
	echo -e "\x1b[91mERROR: \x1b[m ~/op/API_KEY not found" >&2
	exit 1
fi
cd ~/.op/

function txt2json() {
	echo -n "$*" | jq -Rs .
}

MSG='{"role": "system", "content": "You are a helpful assistant that give linux command lines as exemple if needed."}'
while :; do
	if [ $# -eq 0 ]; then
		TEXT=""
		echo -e "Enter input (press Ctrl+D to finish):"

		while IFS= read -er line; do
			# Append each line to the input variable
			TEXT+="$line"

			# Add a newline character to separate the lines
			TEXT+=$'\n'
		done
	else
		TEXT="$*"
		while [ $# -gt 0 ]; do shift; done
	fi
	if ! [ "$TEXT" ]; then
		echo bye
		exit
	fi
	TEXT="$(txt2json "$TEXT")"
	MSG+=',{"role": "user","content":'"${TEXT}"'}'
	echo
	echo "Asking OpenIA...."
	echo
	CURLRST="$(curl -s https://api.openai.com/v1/chat/completions \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $(cat API_KEY)" \
		-d '{
  "model": "gpt-4",
  "messages": [
     '"$MSG"'
  ],
  "temperature": 1,
  "max_tokens": 1024,
  "top_p": 1,
  "frequency_penalty": 0,
  "presence_penalty": 0
}')"
	RST="$(echo "$CURLRST" |
		jq .choices[0].message.content)"
	MSG+=',{"role": "assistant", "content":'"${RST}"'}'
	echo -e "$RST" | sed 's/\\"/"/g' | sed 's/^"//' | sed 's/$"//' |
		awk '
		    /```/ {col=!col ; printf col?"\x1b[m\x1b[32m":"\x1b[m"}
		    {
			    printf col?"":"\x1b[1m" ;
			    print
		    }' |
		sed 's/\([^`]\)\(`[^`]*`\)\([^`]\)/\1\x1b[32m\2\x1b[m\x1b[1m\3/g'
	echo
done
