# README

This script is designed to interact with the OpenAI API using the GPT-3.5-turbo model. It allows users to input a text query, sends the query to OpenAI, and returns a response from the model.

## Usage

To run this script, use the following command:

```
./script.sh [input]
```

If `[input]` is not provided, the script will prompt the user to enter the input and press Ctrl+D to finish. The input can be multiple lines, and each line will be appended to the `TEXT` variable.

The script first checks if any arguments are provided. If no arguments are given, it reads the input from the user and stores it in the `TEXT` variable. It uses a `while` loop and `read` command to read each line and append it to `TEXT` with a newline character.

If arguments are provided, the script treats them as the input and stores them directly in the `TEXT` variable.

The `TEXT` variable is then normalized using the `jq` tool. The command `$(echo -n "$TEXT" | jq -Rs .)` removes any special characters and escapes, making the input suitable for the API request.

The script then sends an API request to OpenAI using the `curl` command. The request includes the `TEXT` as the user message for the conversation. The following parameters are used in the request:

- `model`: Specifies the GPT-3.5-turbo model to use.
- `messages`: An array containing a single message object with the role set as "user" and the content set as the input text.
- `temperature`: Controls the randomness of the generated response.
- `max_tokens`: Specifies the maximum number of tokens in the response.
- `top_p`: A threshold value for the cumulative probability of tokens to consider.
- `frequency_penalty`: Penalty to reduce the likelihood of repetitive responses.
- `presence_penalty`: Penalty to reduce the likelihood of generating non-contextual responses.

The API response is then processed using `jq` to extract the content of the response message. The response is further processed using `sed` to remove any escape characters and quotation marks from the beginning and end of the message.

Finally, the response is formatted using `awk` to highlight code blocks enclosed in backticks and using colors to differentiate them. The script also adds formatting to make the output more readable.

## Dependencies

This script requires the following dependencies:

- `jq`: A lightweight and flexible command-line JSON processor.
- `curl`: A command-line tool for making HTTP requests.

Ensure that these dependencies are installed and accessible in the system's `$PATH`.

## Output

The script outputs the response from the OpenAI API. The response includes both the original input and the generated response from the model. The response is formatted to highlight code blocks and provide improved readability.

## Authorization

The script requires an authorization token to access the OpenAI API. The token should be stored in a file named `hw2` in the same directory as the script. This token will be used as the `Authorization` header in the API request.

Ensure that the `hw2` file contains a valid authorization token.

## Limitations

- This script is designed to work with the GPT-3.5-turbo model. If you want to use a different model, you will need to modify the `model` parameter in the API request.
- The script has a maximum limit of 1024 tokens for the generated response. If the response exceeds this limit, it will be truncated.
- The script assumes the presence of the `jq` and `curl` commands in the system's `$PATH`. If these commands are not available, the script will not function correctly.

## Example

```
./script.sh "What is the capital of France?"
```

```
./script.sh
Enter input (press Ctrl+D to finish):
What is the capital of France?
^D
```

## Note

This script is provided as-is and may require modifications based on your environment or specific use case.
